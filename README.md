# iocp-dotnet-iot

#### 介绍 
   编写支持高并发性的网络服务器，瓶颈往往出在I/O上，目前最高效的是采用Asynchronous I/O模型，Windows平台提供了I/O Completion Port(IO完成端口，即IOCP)，相比Linux平台下的epoll、苹果平台下的kqueue、Solaris平台下的evpoll，IOCP更有优势。接下来就来探索IOCP，dotnet用SocketAsyncEventArgs类对iocp进行了封装，我们不需要去理解内核对象IOCP，极大的简化了开发，SocketAsyncEventArgs 主要特点是可以避免在异步套接字 I/O 量非常大时发生重复的对象分配和同步。
#### 网络结构

请求流量经LVS转发给后端的ECS，回程流量经LVS转发给Client。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/121606_a53ede1a_849171.jpeg "四层转发原理.jpg")
#### 数据上报、命令下发
##### 数据上报
当设备完成和IOT平台对接后，一旦设备上电，设备基于在设备上定义的业务逻辑进行数据采集和上报，可以是基于周期或者事件触发。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0211/174537_51ce10ab_849171.png "数据上报.png")
##### 指令下发
支持应用服务器通过调用API接口向单个设备下发命令，以实现对设备的远程控制。需要应用服务器侧和设备侧对命令名称和命令参数做好约定。物联网平台有两种命令下发机制，立即下发、缓存下发。
##### 立即下发、
不管设备是否在线，平台收到命令后立即下发给设备。如果设备不在线或者设备没收到指令则下发失败。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0213/151858_a9204202_849171.png "立即下发.png")
##### 缓存下发
IOT平台在收到命令后先缓存，等设备上线或者设备上报数据时再下发给设备，目前支持缓存单条命令。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0213/151911_afc22f11_849171.png "指令-缓存下发.png")


#### 使用 SocketAsyncEventArgs 类创建高性能网络服务器

1.  分配一个新的 SocketAsyncEventArgs 上下文对象，或者从应用程序池中获取一个空闲的此类对象。
2.  将该上下文对象的属性设置为要执行的操作（例如，完成回调方法、数据缓冲区、缓冲区偏移量以及要传输的最大数据量）。
3.  调用适当的套接字方法 (xxxAsync) 以启动异步操作。
4.  如果异步套接字方法 (xxxAsync) 返回 true，则在回调中查询上下文属性来获取完成状态。
5.  如果异步套接字方法 (xxxAsync) 返回 false，则说明操作是同步完成的。 可以查询上下文属性来获取操作结果。
6.  将该上下文重用于另一个操作，将它放回到应用程序池中，或者将它丢弃。

#### 通讯服务器创建流程
